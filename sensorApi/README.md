# BACKEND

## Setup without pypoetry

```powershell
python -m venv venv
.\venv\Scripts\activate
pip install -r requirements.txt
```

## Setup with pypoetry

```powershell
poetry install
```

## Usage

> note: for poetry prepend command with `poetry run`

### Database setup

```powershell
python manage.py migrate
python manage.py loaddata api.json
```

### Start development server

```powershell
python manage.py runserver 0.0.0.0:8000
```

### Run test suite

```powershell
pytest
```

coverage reports will be automatically generated:

- html in `htmlcov` folder
- xml in root folder
