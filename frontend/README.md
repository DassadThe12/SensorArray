# FRONTEND

## Project setup

```powershell
npm install
```

### Run development server

```powershell
npm run serve
```

### Run test suite

```powershell
npm run test
```

Coverage reports will be generated in `coverage` folder.

- clover.xml is in coverage folder
- html report is in coverage/lcov-report folder
