import axios from "axios";
import { SensorDescription } from "@/types";

export default class SensorListService {
  url = "http://localhost:8000/api/sensors_list/";
  lastData: SensorDescription[];
  constructor(data?: SensorDescription[]) {
    if (data !== undefined) {
      this.lastData = data;
    } else {
      this.lastData = [];
    }
  }

  getData() {
    return axios.get(this.url).then((data) => {
      this.lastData = data.data;
      return data;
    });
  }
}
