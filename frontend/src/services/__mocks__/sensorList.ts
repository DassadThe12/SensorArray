import { SensorDescription } from "@/types";

export default class SensorListService {
  constructor(data: SensorDescription[]) {
    this.lastData = data;
  }
  lastData: SensorDescription[];
  getData() {
    return new Promise((resolve) => {
      resolve({ data: this.lastData });
    });
  }
}
