import axios from "axios";
import IsingleSensorData from "@/types";
import makeParams from "@/services/utils/makeParams";

export default class SingleSensorData {
  url = "http://localhost:8000/api/log_data/";
  lastData: IsingleSensorData[];
  constructor() {
    this.lastData = [];
  }

  updateData(sensorId: string, start = -1, end = -1) {
    const params = makeParams(start, end);
    return axios
      .get(this.url + sensorId + "/", { params: params })
      .then((data) => {
        this.lastData = data.data;
        return data;
      });
  }
}
