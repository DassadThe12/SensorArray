export default function makeParams(start: number, end: number) {
  let params = {};
  if (start > 0) {
    const add = { start: (start / 1000).toFixed(0) };
    params = { ...params, ...add };
  }
  if (end > 0) {
    const add = { end: (end / 1000).toFixed(0) };
    params = { ...params, ...add };
  }
  return params;
}
