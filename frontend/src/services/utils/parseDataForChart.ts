import IsingleSensorData from '@/types';

export default function parseDataForChart(lastData: IsingleSensorData[]) {
  const dates = lastData.map((e) => {
    return new Date(e.timestamp);
  });

  const temperatures = lastData.map((e) => {
    return e.temperature;
  });
  const humidities = lastData.map((e) => {
    return e.humidity;
  });

  let ret = {
    labels: dates,
    datasets: [
      {
        label: "Temperature",
        fill: false,
        backgroundColor: "#2f4860",
        borderColor: "#2f4860",
        yAxisID: "temp-axis",
        data: temperatures,
      },
      {
        label: "Humidity",
        fill: false,
        backgroundColor: "#00bb7e",
        borderColor: "#00bb7e",
        yAxisID: "humid-axis",
        data: humidities,
      },
    ],
  };
  return ret;
}
