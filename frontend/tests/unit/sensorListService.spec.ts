import SensorListService from "@/services/sensorList";
import { SensorDescription } from "@/types";
import axios from "axios";

jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe("SensorList Service", () => {
  const data: { data: SensorDescription[] } = {
    data: [
      {
        description: "d",
        display_name: "n",
        id: 1,
        name: "1234asd",
      },
      {
        description: "D",
        display_name: "N",
        id: 2,
        name: "1234ASD",
      },
    ],
  };
  it("Returns a list of sensors from backend and saves it in lastData field", async () => {
    const service = new SensorListService();

    mockedAxios.get.mockImplementationOnce(() => Promise.resolve(data));

    await expect(service.getData()).resolves.toMatchObject(data);
  });
  it("lastData is empty list when no arguments passed to constructor", async () => {
    const service = new SensorListService();
    expect(service.lastData).toMatchObject([]);
  });

  it("lastData is set to data passed in constructor", async () => {
    const service = new SensorListService(data.data);
    expect(service.lastData).toMatchObject(data.data);
  });


});
