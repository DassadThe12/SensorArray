import SensorListService from "@/services/sensorList";
import SingleSensorData from "@/services/singleSensorData";
import axios from "axios";
import IsingleSensorData from "@/types";

jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe("SensorList Service", () => {
  const data: { data: IsingleSensorData[] } = {
    data: [
      { humidity: 1, temperature: 1, timestamp: "4" },
      { humidity: 2, temperature: 2, timestamp: "5" },
      { humidity: 3, temperature: 3, timestamp: "6" },
    ],
  };
  afterEach(() => {
    mockedAxios.get.mockClear()
  });
  it("Requests sensor data from backend and caches it in lastData field", async () => {
    const service = new SingleSensorData();

    mockedAxios.get.mockImplementationOnce(() => Promise.resolve(data));

    await expect(service.updateData("aas")).resolves.toMatchObject(data);
    expect(service.lastData).toMatchObject(data.data);
  });

  it("Adds params to query if start and end greater than 0", async () => {
    const service = new SingleSensorData();

    mockedAxios.get.mockImplementationOnce(() => Promise.resolve(data));

    service.updateData("a", 1, 1);
    expect(axios.get).toBeCalledWith(service.url + "a/", {
      params: { start: "0", end: "0" },
    });
  });
});
