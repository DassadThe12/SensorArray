import makeParams from "@/services/utils/makeParams";
import IsingleSensorData from "@/types";
import parseDataForChart from "@/services/utils/parseDataForChart";

// testy do serwisu, konkretnie parseDataForChart i makeParams

describe("MakeParams", () => {
  it("Retruns an object with parameters ready for axios get request", () => {
    let data = makeParams(1, 1);
    expect(data).toMatchObject({ start: "0", end: "0" });
  });
  it("given start is less than 0 do not add it to the list of params", () => {
    let data = makeParams(-1, 1);
    expect(data).toMatchObject({ end: "0" });
  });
  it("given end is less than 0 do not add it to the list of params", () => {
    let data = makeParams(1, -1);
    expect(data).toMatchObject({ start: "0" });
  });
  it("given less than 0 in both params return an empty object", () => {
    let data = makeParams(-1, -1);
    expect(data).toMatchObject({});
  });
});
describe("ParseDataForChart", () => {
  let basicSettings: {
    labels: Date[];
    datasets: {
      label: string;
      fill: boolean;
      backgroundColor: string;
      borderColor: string;
      yAxisID: string;
      data: number[];
    }[];
  };
  let mockData: IsingleSensorData[];
  beforeEach(() => {
    basicSettings = {
      labels: [],
      datasets: [
        {
          label: "Temperature",
          fill: false,
          backgroundColor: "#2f4860",
          borderColor: "#2f4860",
          yAxisID: "temp-axis",
          data: [],
        },
        {
          label: "Humidity",
          fill: false,
          backgroundColor: "#00bb7e",
          borderColor: "#00bb7e",
          yAxisID: "humid-axis",
          data: [],
        },
      ],
    };
    mockData = [
      { humidity: 1, temperature: 1, timestamp: "4" },
      { humidity: 2, temperature: 2, timestamp: "5" },
      { humidity: 3, temperature: 3, timestamp: "6" },
    ];
  });
  it("given no data returns basic settings with empty arrays in labels and data fields", () => {
    let result = parseDataForChart([]);
    expect(result).toMatchObject(basicSettings);
  });
  it("given an array of IsingleSensorData return an object with correctly parsed data for chart", () => {
    basicSettings.labels = [new Date("4"), new Date("5"), new Date("6")];
    basicSettings.datasets[0].data = [1, 2, 3];
    basicSettings.datasets[1].data = [1, 2, 3];
    let result = parseDataForChart(mockData);
    expect(result).toMatchObject(basicSettings);
  });
});
