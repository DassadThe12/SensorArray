import { shallowMount, createLocalVue } from "@vue/test-utils";
import VueRouter from "vue-router";
import sensorList from "@/views/sensorList.vue";
import flushPromises from "flush-promises";

import sensorListService from "../../src/services/sensorList";
import { VueConstructor } from "vue";
jest.mock("../../src/services/sensorList");

let localVue: VueConstructor<Vue>;
let router: VueRouter;

beforeEach(() => {
  localVue = createLocalVue();
  localVue.use(VueRouter);
  router = new VueRouter();
});

describe("SensorList", () => {
  it("Renders a list of sensors from data in props.", async () => {
    const service = new sensorListService([
      {
        id: 1,
        name: "tm1xpgiizrinoxo6w2zd",
        display_name: "Sensor 1",
        description: "Data collected 2020-05-07 to 2020-05-08",
      },
      {
        id: 2,
        name: "7jisve665ybr8jbplhqz",
        display_name: "Sensor2",
        description: "Collection from 2020-05-11 to 2020-05-12",
      },
    ]);
    const wrapper = shallowMount(sensorList, {
      data() {
        return {
          service,
        };
      },
      localVue,
      router,
    });
    await flushPromises();

    let tableData = wrapper
      .find("table.sensorList")
      .findAll("tr.data")
      .wrappers.map((e, index) => {
        const id = index + 1;
        const name = e.find(".name").text();
        const display_name = e.find(".display_name").text();
        const description = e.find(".description").text();
        return {
          id,
          name,
          display_name,
          description,
        };
      });
    expect(tableData).toStrictEqual(service.lastData);
  });

  it("Hides the table if no sensors data in sensors array", async () => {
    const service = new sensorListService([]);
    const wrapper = shallowMount(sensorList, {
      data() {
        return {
          service,
        };
      },
      localVue,
      router,
    });
    await flushPromises();
    expect(wrapper.find("table.sensorList").isVisible()).toBeFalsy();
  });
});
